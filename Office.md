## Office

## 1. ActiveX名称

> 名称

| **控件名** | **name**               |
| ---------- | ---------------------- |
| WPS文字    | KWPS.Aplication        |
| WPS表格    | KET.Application        |
| WPS演示    | KWPP.Application       |
| Word       | Word.Application       |
| Excel      | Excel.Application      |
| PowerPoint | Powerpoint.Application |

> 调用

```c#
using Ppt = Microsoft.Office.Interop.PowerPoint;
using Word = Microsoft.Office.Interop.Word;
var pptApp = Marshal.GetActiveObject("PowerPoint.Application") as Ppt.Application;
```

## 2. 调用

| 功能        | Word                                                         | Excel                                                        | PPT                                                          |
| ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 命名空间    | Microsoft.Office.Interop.Word                                | Microsoft.Office.Interop.Excel                               | Microsoft.Office.Interop.PowerPoint                          |
| 实例化app   | new Word.Application();                                      | new Excel.Application();                                     | new Ppt.Application()                                        |
| 打开文档    | var document = wordApplicationIns.Documents.Open(ref objFullName, ref paramMissing, ref paramMissing   ref paramMissing, ref paramMissing, ref paramMissing,    ref paramMissing, ref paramMissing, ref paramMissing,    ref paramMissing, ref paramMissing, ref paramMissing,    ref paramMissing, ref paramMissing, ref paramMissing,    ref paramMissing); | var workbook = excelApplicationIns.Workbooks.Open(fullName); | pptApplicationIns.Presentations.Open(fullName, OFFICECORE.MsoTriState.msoFalse, OFFICECORE.MsoTriState.msoFalse,                        OFFICECORE.MsoTriState.msoTrue); |
| 文档集合    | wordApplicationIns.Documents                                 | excelApplicationIns.Workbooks                                | pptApplicationIns.Presentations                              |
| 关闭文档    | document.Close();                                            | workbook.Close();                                            | presentation.Close();                                        |
| 关闭app     | wordApplicationIns.Quit();                                   | excelApplicationIns.Quit();                                  | pptApplicationIns.Quit();                                    |
| com释放     | Marshal.FinalReleaseComObject(application);                  |                                                              |                                                              |
| missing对象 | object paramMissing = Type.Missing;                          |                                                              |                                                              |

## 3. 窗口句柄特征

| 差异点     | 微软office                     | wps整合模式                     | wps多组件模式                   |
| ---------- | ------------------------------ | ------------------------------- | ------------------------------- |
| 最外层类名 | PPTFrameClass、OpusApp、XLMAIN | PP12FrameClass、OpusApp、XLMAIN | PP11FrameClass、OpusApp、XLMAIN |
|            |                                |                                 |                                 |

### 3.1 wps多组件模式窗口层次

```bash
# 打开多个ppt场景：括号内为类名，第三级会有多个
测试ppt.pptx - WPS 演示 (PP11FrameClass)
└───(MDIClient)
	└───新建 Microsoft PowerPoint 演示文稿.pptx (mdiClass)
	└───test.pptx (mdiClass)
# 打开多个word场景：括号内为类名，第三级会有多个
word2.docx - WPS 文字 (OpusApp)
└───(_WwF)
	└───word1.docx (_WwB)
	└───word2.docx (_WwB)
# 打开多个excel场景：括号内为类名，第三级会有多个
3.xlsx - WPS 表格 (XLMAIN)
└───(XLDESK)
	└───1.xlsx  (EXCEL7)
	└───2.xlsx  (EXCEL7)
# 放映多个ppt场景：第一级会有多个
WPS演示 幻灯片放映 - [测试ppt.pptx] (Qt5QWindowIcon)
test.pptx (mdiClass)
```

### 3.2 wps整合模式窗口层次

```bash
# 第一个打开的为ppt，2ppt、2word、2excel场景
word2.docx - WPS Office (PP12FrameClass)
└───(PP12FrameClass)
│	└───(PP12FrameClass)
│	    └────后续层级与多组件模式的ppt一致
└───(PP12FrameClass)
│	└───(PP12FrameClass)
│	    └────后续层级与多组件模式的word一致
└───(PP12FrameClass)
	└───(PP12FrameClass)
	    └────后续层级与多组件模式的excel一致
# 第一个打开的为word，2ppt、2word、2excel场景
word2.docx - WPS Office (OpusApp)
└───(OpusApp)
│	└───(OpusApp)
│	    └────后续层级与多组件模式的ppt一致
└───(OpusApp)
│	└───(OpusApp)
│	    └────后续层级与多组件模式的word一致
└───(OpusApp)
	└───(OpusApp)
	    └────后续层级与多组件模式的excel一致
# 第一个打开的为excel，2ppt、2word、2excel场景
word2.docx - WPS Office (XLMAIN)
└───(XLMAIN)
│	└───(XLMAIN)
│	    └────后续层级与多组件模式的ppt一致
└───(XLMAIN)
│	└───(XLMAIN)
│	    └────后续层级与多组件模式的word一致
└───(XLMAIN)
	└───(XLMAIN)
	    └────后续层级与多组件模式的excel一致
# 放映多个ppt场景：第一级会有多个
WPS演示 幻灯片放映 - [测试ppt.pptx] (Qt5QWindowIcon)
```

### 3.3 微软office

```bash
# 打开多个ppt场景：括号内为类名，第一级会有多个
# 注意ppt第三级标题为空，word和excel不为空
1000100000002620143_新建 PPTX 演示文稿 - PowerPoint (PPTFrameClass)
└───(MDIClient)
	└───(mdiClass)
test - PowerPoint (PPTFrameClass)
└───(MDIClient)
	└───(mdiClass)
# 打开多个word场景：括号内为类名，第一级会有多个
test文档 1 - Word (OpusApp)
└───(_WwF)
	└───test文档 1 (_WwB)
test文档 2 - Word (OpusApp)
└───(_WwF)
	└───test文档 2 (_WwB)
# 打开多个excel场景：括号内为类名，第一级会有多个
test表格 1 - Excel (XLMAIN)
└───(XLDESK)
	└───test表格 1 (EXCEL7)
test表格 2 - Excel (XLMAIN)
└───(XLDESK)
	└───test表格 2 (EXCEL7)
# 放映多个ppt场景：第一级会有多个
PowerPoint 幻灯片放映 - [1000100000002620143_新建 PPTX 演示文稿] (screenClass)
PowerPoint 幻灯片放映 - [test] (screenClass)
```



## 4. Office不同版本差异

### 4.1 office ocx版本号

```bash
# typelib dll
HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Classes\TypeLib\{00020905-0000-0000-C000-000000000046}
```

| office版本 | dll版本号 |
| ---------- | --------- |
| 2003       | 11        |
| 2007       | 12        |
| 2010       | 14        |
| 2013       | 15        |
| 2016       | 16        |



### 4.2 office2010

#### 4.2.1 excel2010 标题

```c#
// excel 2010
Microsoft Excel - c3a9bdbe-a547-4608-9c72-ed014d4961a6.xlsx
// excel 2013 2016 2019
c3a9bdbe-a547-4608-9c72-ed014d4961a6.xlsx - Excel
```

#### 4.2.2 word2010 excel2010 com接口获取窗口句柄报错

```c#
// excel2010无法通过com接口获取窗口句柄
// System.Runtime.InteropServices.COMException (0x80020003): 找不到成员。 (异常来自 HRESULT:0x80020003 (DISP_E_MEMBERNOTFOUND)
window.Hwnd
    
// word2010无法通过com接口获取窗口句柄
// System.AccessViolationException 尝试读取或写入受保护的内存。这通常指示其他内存已损坏
window.Hwnd
```

#### 4.2.3 ppt2010放映时窗口层次

```c#
// ppt2010
screenClass --- paneClassDC
// ppt2013 2016 2019
screenClass
```

### 4.3 office2021

#### 4.3.1 excel2021 Count个数为0、无法关闭

```c#
// 个数始终为0
// 由于个数为0，无法获取对应window导致无法关闭
var n1 = app.Workbooks.Count;
var n2 = app.Windows.Count;
```



### 4.3. wps 整合模式 多组件模式

#### 4.3.1 wps左上角标签名称

```c#
// 整合模式
首页
// 多组件模式
WPS 文字、WPS 表格、WPS演示
```

#### 4.3.2 第一层窗口标题

```c#
// 整合模式
 - WPS Office
// 多组件模式
 - WPS 文字、 - WPS 表格、 - WPS演示
```

#### 4.3.3 第一层窗口类名

```c#
// 整合模式
PP12FrameClass、OpusApp、XLMAIN
// 多组件模式
PP11FrameClass、OpusApp、XLMAIN
```



### 4.4 word、excel、ppt ocx

```bash
document.FullName
workBook.FullName
presentation.FullName
```





## 5. com ocx

```bash
# msoffice
C:\Program Files (x86)\Microsoft Office\Office14\MSWORD.OLB
C:\Program Files (x86)\Microsoft Office\Office14\EXCEL.EXE
C:\Program Files (x86)\Microsoft Office\Office14\MSPPT.OLB
# wps
C:\Users\Administrator\AppData\Local\kingsoft\WPS Office\11.1.0.12763\office6\wpsapi.dll
C:\Users\Administrator\AppData\Local\kingsoft\WPS Office\11.1.0.12763\office6\etapi.dll
C:\Users\Administrator\AppData\Local\kingsoft\WPS Office\11.1.0.12763\office6\wppapi.dll
```

```bash
# tlbimp.exe aximp.exe
# C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6 Tools\tlbimp.exe
tlbimp.exe "C:\Program Files (x86)\Microsoft Office\Office14\MSWORD.OLB"
```



## 6. ppt快速放映

```bash
1.将ppt或pptx改为pps或ppsx （微软office下改为pps）
2.双击打开，直接进入幻灯片放映模式，并且文件不会打开
```



## 7. ppt Deactive

```bash
# a关闭
a失去焦点->a关闭
# 打开b
a失去焦点
# a切换到b
a失去焦点
# a最小化
a失去焦点
# a开始放映
a失去焦点->a开始放映
# a结束放映
a结束放映->a失去焦点
```



## 8. wpp.exe进程

```bash
# wpp.exe退出慢
1. 打开ppt文件，程序获取ppt实例
2. 关闭ppt，关闭程序，wpp.exe 30s后退出
# 获取wpp.exe进程打开的ppt文件
- 通过process explorer查看进程的文件句柄
- 通过handle.exe查看进程的文件句柄
# wps切标签方式
- ctrl+shift+tab挨个切换标签
- 打开同一个文件
- 向缩略图窗口发送SetForeground(有bug)
- 调用com接口window.Activate(不能跨word/excel/ppt)
```



## 9. office参数

```bash
# 不显示loading窗体
excel /e "C:\Users\Administrator\Desktop\123\1.xlsx"
```



## 10. office报错

### 10.1 Presentation (unknown member) : Invalid request.  Automation rights are not granted.

```bash
安装office时未勾选"Office 工具/标记.NET 可编程性支持"
```

