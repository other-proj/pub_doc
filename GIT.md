## 0. 使用错误的地方

- git stash (新增的文件不会被暂存)
> 正确用法
```bash
git stash -u
```
- git reset + git push -f 撤回服务器代码
> 正确用法
```bash
git revert head
```
- git pull origin dev (拉取代码有可能产生自动合并记录merge)
> 优化用法
```bash
git pull origin dev --rebase
```

## 1. clone

### 1.1 克隆最后一次commit
```bash
git clone --depth=1 https://gitee.com/user1/proj1.git
```
### 1.2 克隆指定分支
```bash
git clone -b dev https://gitee.com/user1/proj1.git
```
> 备注
- -b --depth可组合使用

### 1.3 git仓库迁移
```bash
git clone --mirror https://gitee.com/user1/proj1.git
cd proj1.git
git push --mirror https://gitee.com/user2/proj1.git
```

> 备注
- 可保留：commit、branch、tag
- 无法保留：release、attachment
- git clone --mirror 可以改为 git clone --bare
- 目录内容跟.git目录类似
- 无法使用git切换分支等命令，必须按正常重新克隆代码

### 1.4 git仓库迁移commit记录

```bash
# 本地项目文件夹A、B
# A为旧项目，B为新项目，想把B中dev分钟合并到A的dev中并保留commit
git remote add origin2 ../B
git fetch --all
git merge origin2/dev
```



## 2. branch

### 2.1 根据远程dev创建本地dev分支
```bash
# 查看所有分支（包括remote）
git branch -a
# 根据remote创建本地分支
git checkout -b dev remotes/origin/dev
```

```bash
git checkout --track origin/dev
```

### 2.2 删除分支

```bash
# 删除本地分支
git branch -d dev
# 删除服务器分支
git push origin -d dev
```

### 2.3 删除本地remote有，远程remote没有的本地分支
```bash
git fetch origin --prune
```


### 2.4  pre-receive hook decline分支被保护

```bash
! [remote rejected] dev -> dev (pre-receive hook declined)
```


## 3. tag
### 3.1 推送单个标签
```bash
git push origin tags/110封版
```

### 3.2 推送所有标签
```bash
git push origin --tags
```

### 3.3  [rejected]          v2.1.0     -> v2.1.0  (would clobber existing tag)

```bash
# 原因：
# 删了原有的一个tag，然后重新创建了一个相同名字的
git pull origin --tags
```

```bash
#解决：
git pull origin --tags -f
# 或
git fetch origin --tags -f
```



## 4. rebase

### 4.1 pull产生merge commit问题
```bash
# 如果直接pull，有可能会产生merge记录
git stash
git pull origin dev --rebase
git stash pop
```

### 4.2 分支合并
```bash
# 将fix分支调整为以master为基准
git checkout fix
git rebase master
# 将fix合并到master
git checkout master
git merge fix
```

## 5. commit
### 5.1 代码已提交服务器，撤回 revert
```bash
# 还原最后一次提交
git revert head
````
```bash
# 还原倒数第二次提交
# 撤回中间提交的代码（不建议）
git revert head~1
# 可以跨commit撤回，要采用后进先出的方式依次撤回
git revert <commit-id>
````
### 5.2 代码已提交到本地，撤回 reset

```bash
# 还原到74148fb6a0d771b263a42723d2e742867107876b次提交，之后提交的记录和代码全部删除
git reset 74148fb6a0d771b263a42723d2e742867107876b --hard
# 还原到74148fb6a0d771b263a42723d2e742867107876b次提交，之后提交的记录删除、代码保留
git reset 202ef441e3ccb4801a144a9d6a5394498840ef56 --mixed 
```

### 5.3 修改commit注释 commit amend

```bash
# 最后一条commit
git commit --amend
# 还原
git rebase --continue
```
```bash
# 任意一条commit
# 将需要修改的commit中的pick改为edit，并wq
git rebase -i HEAD~2  
# 后续操作与修改最后一条一样
git commit --amend
# 还原
git rebase --continue
```

### 5.4 修改commit日期 commit amend

```bash
# 修改最后一条commit date
# commit date可以通过--amend或者.patch文件获取
git commit --amend --date="Sat, 13 Aug 2022 18:13:22 +0800"
```

### 5.5 修改commit作者 commit amend

```bash
# 修改最后一条commit 作者
# commit date可以通过--amend或者.patch文件获取
git commit --amend --author="xiaohei <xiaohei@qq.com>"
```

### 5.6 修改comit日期、作者(工具)

```bash
# 情况1：修改上一次记录
tortoist git->git commit->勾选amend lastcommit、set author date、set author，修改，提交
```

```bash
# 情况2：修改之前某次记录
[1]fork->右键interactive rebase->edit->rebase
[2]tortoist git->git commit->勾选amend lastcommit、set author date、set author，修改，提交
[3]fork->amend last commit->continue rebase
```

### 5.7  代码提错分支

```bash
# 将dev2中的commit1复制到dev中：
# 切换到dev，获取commit1的id，执行下面的语句
git cherry-pick <commitHash>
```

### 5.8 patch导出与导入

```bash
# 导出
fork->save as patch
# 导入
tortoise git->apply patch serial->add (选择.patch文件)->apply
```



## 6. stash

```bash
git stash -u
git stash pop
```

```bash
# 缓存所有改、删
git stash
# 缓存所有改、删、增
git stash -u
# 缓存单个文件改、删（交互方式选择哪个stash、哪个不stash）
git stash -p
# 释放上一个缓存
git stash pop
# stash-查 (relative（几分钟前）、local（年月日时分秒）、short（年月日）)
git stash list --date=local
```

## 7. 账号配置
### 7.1 创建ssh文件
```bash
# 方式一
ssh-keygen
```

```bash
# 方式二
ssh-keygen -t rsa -C user1@qq.com
```
> 备注
```bash
# 默认路径
%userprofile%\.ssh
# 私钥文件
id_rsa
# 公钥文件
id_rsa.pub
```

### 7.2 两个gitee账号ssh配置

> config文件中，Host为别名

> %userprofile%\.ssh\config文件内容：

```bash
# test1
Host test1.gitee.com
HostName gitee.com
IdentityFile ~\.ssh\gitee-test1

# test2
Host test2.gitee.com
HostName gitee.com
IdentityFile ~\.ssh\gitee-test2
```

> 测试连通性：

```bash
# 测试frozelaf
ssh -T git@test1.gitee.com

# 测试zzlh
ssh -T git@test2.gitee.com
```

### 7.3 git 私钥转ppk

1. 打开PuTTYgen

2. 菜单Conversations->Import key
3. Save private key

### 7.4 git unrecognised cipher name

原因
> ssh-keygen新版本生成私钥格式，PuTTYgen不支持

> 旧版私钥格式
```bash
-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: AES-128-CBC,6E0BD32EFC8CB766260A8CBD750FEABE

-----END RSA PRIVATE KEY-----
```
> 新版私钥格式
```bash
-----BEGIN OPENSSH PRIVATE KEY-----

-----END OPENSSH PRIVATE KEY-----
```
解决
> 通过ssh-keygen转换为旧版格式
```bash
# 转换后会替换源文件
ssh-keygen -p -m pem -f /c/users/tudou/.ssh/myprikey
```

## 8. repository
### 8.1 全部删除
通过git服务器清空项目数据

### 8.2 只保留最初的两条commit
通过强制推送数据
```bash
git init
git remote add origin <url>
touch README.md
git add .
git commit -m 'delete'
# 强制‘清空’分支：master
git push origin master --force
# 删除远程分支：dev
git push origin -d dev
# 删除远程标签：1.2.0
git push origin :refs/tags/1.2.0
```

## 9. 大文件

### 9.1 拉取
> 文件太大拉取报错

方式一：采用ssh链接

方式二：修改http.postBuffer大小
```bash
# 修改为500MB
git config http.postBuffer 524288000
```

### 9.2 推送
#### 9.2.1 文件太大，但未超过上限
采用ssh链接

#### 9.2.2 文件太大，超过gitee单文件上限

> 从git错误信息入手

```bash
# 1.从git错误信息中获取超大文件的id
remote: error: File: 00fa9fa3cb94cd472267a3a3e0a41cc7e71d9b9c 146.10 MB, exceeds 100.00 MB.    
# 2.查找文件路径
git rev-list --objects --all | grep 00fa9fa3cb94cd472267a3a3e0a41cc7e71d9b9c
# 3.删除文件
# 注意路径包含空格，要使用双引号
git filter-branch --tree-filter 'rm -f "安装包制作/版本/0.5/xxx.exe"' --tag-name-filter cat -- --all
# 删除文件结果，“was rewritten”表示修改成功
Ref 'refs/heads/dev' was rewritten
1.1.0 -> 1.1.0 (ab3d71596a421475c9e98243dcf4be6e5e605cf5 -> ab3d71596a421475c9e98243dcf4be6e5e605cf5)
1.1.0_final -> 1.1.0_final (9e5a750fe11a8df15c0dfd599b4ee0eca99e0e68 -> 9e5a750fe11a8df15c0dfd599b4ee0eca99e0e68)
1.1.0_fix01 -> 1.1.0_fix01 (2748aea63494cdbf7b77ea1f397e819810077753 -> 2748aea63494cdbf7b77ea1f397e819810077753)
v1.2.0 -> v1.2.0 (decb589d656b74ec21049f6d4ef2e8e808e01a1e -> decb589d656b74ec21049f6d4ef2e8e808e01a1e)
# 4.强制推送
git push --force
```

> 查找最大的前20个文件入手

```bash
# https://blog.csdn.net/qq_26443509/article/details/109176638
# 1.查找最大的前20个文件
git rev-list --all | xargs -rL1 git ls-tree -r --long | sort -uk3 | sort -rnk4 | head -20
# 2.查找文件路径
git rev-list --objects --all | grep 00fa9fa3cb94cd472267a3a3e0a41cc7e71d9b9c
# 3.删除文件
git filter-branch --tree-filter 'rm -f "安装包制作/版本/0.5/xxx.exe"' --tag-name-filter cat -- --all
# 4.强制推送
git push --force
```



## 10. 提交历史

### 10.1 定位某行代码修改记录
```bash
# 定位文件中第19到30行修改记录
# 得到修改记录commit id，然后从日志中查看具体信息
git blame -L 19,30 d:\myProject\main.cs
```

### 10.2 排查代码在哪次commit中新增
```bash
# 从最早的记录开始遍历dev分支
# 找到project\test.cs文件中包含代码"Thread.Sleep(500);"的所有commit，并写入到文件1.txt
git rev-list --reverse --branches dev | xargs -I{} git grep "Thread.Sleep(500);" {} -- "project\test.cs" >1.txt
```

### 10.3 代码恢复 commit

> 可以配合小乌龟的reflog使用

> 强制推送导致的代码丢失

```bash
git reflog
git reset --hard HEAD@{5}
```

> 删除分支导致的代码丢失，找到丢失分支的最后一条commit，重新创建分支

### 10.4 代码恢复 stash

> stash误删除

```bash
# 查找误删的stash记录
# 第一种方式git fsck --lost-found>>1.txt （推荐，记录条数比较少）
# 第二种方式git fsck --unreachable>>2.txt
git fsck --lost-found>>1.txt
# 查看修改内容记录
git show 313678317535fb760a216d8f300f86c8e93a364c
# 恢复
git stash apply 313678317535fb760a216d8f300f86c8e93a364c
```




## 11. PR

1. fork 原项目
2. 下载forked项目到本地，新建分支、修改、提交
3. 原项目，pull requests中新建PR，选择原项目分支和forked项目分支，创建即可



## 12. 文件大小写改名

```bash
git mv test.md Test.md
```



## 13. Tortoise Git

### 13.1 文件重命名后git历史显示不全

```bash
# 解决
Git Log界面，Walk Behavior，勾选Flower Renames
```

### 13.2 查看单个文件git历史时，非相关文件太多不好定位

```bash
# 解决
Git Log界面，View，勾选Hide Unrelated Changed Paths 或者 直接搜索文件名
```



## 14. 常见报错

### 14.1 connect to host github.com port 22: Connection timed out

```bash
# C:\Users\Administrator\.ssh\config
HostName github.com改为 HostName ssh.github.com
```

### 14.2   path is not owned by current user git config --global --add safe.directory

```bash
解决方案：更改文件夹的所有者
注：git config --global --add safe.directory设置无效
```

### 14.3 SSL certificate problem

```bash
# 方案一
# 关闭git的证书验证
git config --global http.sslverify false
# 方案二：
set GIT_SSL_NO_VERIFY=true
git clone https://xxx.git
```

### 14.4 ssh: connect to host github.com port 22: Connection refused

```bash
关闭代理(steam++)
```





## 15. git 提交记录排序问题

```bash
# fork 默认采用的按日期排序，可以通过file-preferences，sort commit修改按提交顺序
# tortoise git等默认按提交顺序排序
```



## 16. 打包工具相关

```bash
# 拉取远程分支和标签到本地，
# 如果本地标签不一致会被替换
# 本地分支内容不会被替换，但本地remote分支会被替换
git fetch --tags --force --verbose --progress -- https://gitee.com/xxx.git +refs/heads/*:refs/remotes/origin/*
# 获取remote dev最新commit id
git rev-parse "refs/remotes/origin/dev^{commit}"
# 获取remote tag 1.1最新commit id
git rev-parse "refs/tags/1.1^{commit}"
# 检出commit id对应的代码
git checkout -f 8042273c50d46f2ee0458f3d3db562d0eca3faab

# 获取远程仓库分支列表
git ls-remote --heads https://gitee.com/xxx.git 
# 获取远程仓库标签列表
git ls-remote --tags http://gitee.com/xxx.git
```



