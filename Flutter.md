# Flutter

### 常用控件

| 类型                                                         | 说明                             |
| ------------------------------------------------------------ | -------------------------------- |
| MaterialApp、StatelessWidget、StatefulWidget(State<T>)       | 应用、无状态、有状态             |
| Scaffold、Row、Column、Center、Container、Expanded           | 通用创建部件、行、列、居中、容器 |
| Text                                                         | 文本                             |
| TextField                                                    | 输入框                           |
| ElevatedButton、TextButton、OutlinedButton、FloatingActionButton | 按钮...悬浮按钮                  |
| ListView(ListTile)                                           | 列表                             |
| Icon、IconButton、CircleAvatar                               | 图标                             |
| GestureDetector                                              | 手势                             |
| RadioListTile                                                | 单选                             |
| AlertDialog                                                  | 提示对话框                       |
| Text Style、ButtonStyle                                      | 样式                             |
| InputDecoration                                              | 装饰                             |
| VerticalDivider                                              | 垂直分割线                       |
| NavigationRail                                               | 垂直导航栏                       |
|                                                              |                                  |

## 0. 初始

### 0.1 环境搭建

```bash
# flutter zip + 配置path
# andrdoid studio + flutter插件
```

### 0.2 编译

```bash
# 编译release
# .\build\windows\x64\runner\Release
flutter build windows --release
```



## 1. A RenderFlex overflowed by 366 pixels on the bottom.超出边界且无法垂直滚动

```dart
用ListView代替Column、Row等
```

## 2. NavigationRail

### 2.1 NavigationRail点击导航栏无法切换

```dart
为处理onDestinationSelected事件
```

### 2.2. NavigationRail为显示导航文字

```dart
设置labelType为all
```

## 3. 背景为黑色

```dart
布局控件改为Scaffold
```

## 4. 窗口

### 4.1 修改windows窗口标题、大小

```dart
windows/runner/main.cpp 修改其中的标题和大小
```

## 5. ListView

### 5.1 Vertical viewport was given unbounded width.

Cannot hit test a render box with no size.

```dart
将Center改为Expanded
```

## 6. 路由

### 6.1 页面传参、获取参数、返回值

```dart
// 命名路由传参
// 非命名路由直接传构造函数
Navigator.pushNamed(context, 'name1', arguments: { 'age':1});
// 获取参数
ModalRoute.of(context)!.settings.arguments;
// 关闭时返回参数
Navigator.of(context).pop({
    'x': 1,
    'y': 'xxxxx',
});
// 命令路由定义
main(){
  runApp(GetMaterialApp(
    // ...
    routes: {
      'home1' : (context) => Home1(mytitle: 'hello',),
      'home2' : (context) => Home2(),
    },
  ));
}
```

## 7. 如何在输出目录增加其他目录

```xml
# pubspec.yaml
  assets:
    - injector/
    - injector/net452/
```

## 8. 常用目录

```dart
// 程序路径
Platform.resolvedExecutable
// 临时文件夹
Directory.systemTemp.path
```

## 9. 如何设置abstarct、protected、static

```dart
abstract class FileModifyBase extends StatefulWidget{
  @protected
  void changeFile(String file);
}

class AppConfigUtil{
  static update(String path, Map<String,String> map){}
}
```

## 10. TextField

### 10.1 设置初始文字

```dart
TextEditingController? textController1 = null;
  @override
  Widget build(BuildContext context) {
    textController1 = TextEditingController(text: widget.file);
    return Column(
      children: [
        TextField(controller: textController1, readOnly: true,),
          ]);
  }
```

