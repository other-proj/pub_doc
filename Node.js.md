# Node.js

## 1. 设置淘宝镜像
```bash
npm config set registry https://registry.npmmirror.com/ -g
```

## 2. github下载包失败

### 2.1 单个包处理

```bash
# electron下载失败
# .\node_modules\@electron\get\dist\cjs
npm config set ELECTRON_MIRROR=https://npmmirror.com/mirrors/electron/
# electron builder下载失败
# .\node_modules\app-builder-lib\out\binDownload.js
npm config set ELECTRON_BUILDER_BINARIES_MIRROR=https://npmmirror.com/mirrors/electron-builder-binaries/
```

### 2.2 批量处理

```bash
# 报错RequestError: unable to verify the first certificate
使用了加速器或代理工具，nodejs调用http接口时默认开启了开启验证服务器ssl证书，
解决方案一：关闭代理，使用npm config set设置一个一个设置，但有的场景下也不太行，比如：node-sass；
解决方案二：在批处理里增加 set NODE_TLS_REJECT_UNAUTHORIZED=0 ，获取设置系统环境变量 NODE_TLS_REJECT_UNAUTHORIZED
```

## 3. 查看npm运行报错日志

```bash
%appdata%\npm-cache\_logs
```

## 4. 还原npm镜像地址
```bash
# 查看npmrc文件路径，通过del删除所有npmrc文件
npm config list
```

## 5. asar解包
```bash
# 安装asar
npm install -g asar
# 将app.asar解包到app文件夹
asar extract app.asar app
# 将app文件夹打包成app.asar
asar pack app app.asar
```

## 6. vscode终端中文乱码

```bash
# 将活动代码页改为utf-8
chcp 65001
```

