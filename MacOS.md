## MacOS

## 1. MacOS快捷键

- shift：向上的箭头
- control：向上的大于号；alt/option：下坡加一横；command：麻花

### 1.1 常用

> command相当于windows的ctrl键

- 访达，打开文件夹：command+o
- 访达，前进：command+上箭头
- 访达，后退：command+下箭头
- 访达，删除文件：command+delete
- 访达，复制文件：command+c --> command+v
- 访达，剪切文件：command+c --> option+command+v
- 访达，重命名文件：回车键
- 访达，快速预览文件：空格键
- 访达，切换视图：command+1、command+2、command+3、command+4
- 类似windows中的运行窗口：command+空格
- 访达，创建快捷方式：拖动文件，option+command

### 1.2 其他
> 与windows类似

- 复制文件、文字：command+c
- 粘贴文件、文字：command+v
- 剪切文字：command+x
- 在文件夹中打开终端：文件夹->右键服务->新建位于文件夹位置的终端窗口
- 中英文切换：capslock键
- 大小写切换：长按capslock键直到灯亮
- 分屏操作：长按最大化按钮，弹出菜单进行选择分屏方式



## 2. MacOS常用命令

- 文件名、目录名不区分大小写（与linux不同）

### 2.1 MacOS目录结构

~~~ bash
波浪号，代表home文件夹，即当前用户文件夹，比如：
~/Downloads
~/Documents
~/Desktop
~/Library
~/Music
~/Movies
~/Pictures
~/Applications

/usr
/usr/local
/usr/local/mysql
/usr/local/bin
/usr/local/share

# 软件用户数据存储位置（微信、有道云等）
/Users/lt/Library/Containers
~~~

### 2.2 将.tar.gz压缩包中的内容解压到~/Downloads/dotnet

~~~
tar zxf 1.tar.gz -C ~/Downloads/dotnet
~~~
> z表示有gzip属性的
> x表示解压
> f表示文件名
> -C表示指定要解压的目录

### 2.3 *.tar *.gz *.tar.gz区别

~~~
*.tar 用 tar –xvf 解压
*.gz 用 gzip -d或者gunzip 解压
*.tar.gz和*.tgz 用 tar –xzf 解压
*.bz2 用 bzip2 -d或者用bunzip2 解压
*.tar.bz2用tar –xjf 解压
*.Z 用 uncompress 解压
*.tar.Z 用tar –xZf 解压
*.rar 用 unrar e解压
*.zip 用 unzip 解压
~~~

### 2.4 环境变量

> 添加环境变量
```bash
vim ~/.bash_profile
# 添加内容
export PATH=$PATH:/usr/local/mysql/bin
```

### 2.5 查看变量

```bash
echo $PATH
echo $SHELL
```