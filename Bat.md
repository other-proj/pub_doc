
## 批处理

### 1. 常用变量

```bash
# 查看当前命令行中的环境变量
set
```

```bash
# .bat文件路径 "D:\Desktop\1.bat"
%0
# .bat文件所在目录 D:\Desktop\
%~dp0
# 传入bat的第一个参数，%1特殊场景下会有双引号
%~1
%1
# 当前工作目录
%cd%
# C:
%systemdrive%
# C:\Windows
%systemroot%
# C:\Users\test\AppData\Roaming
%appdata%
# C:\Users\test\AppData\Local
%localappdata%
# C:\ProgramData
%allusersprofile%
# C:\Users\test\Documents
%userprofile%\Documents
# C:\Users\Public\Documents
%PUBLIC%\Documents
# C:\Users\test
%userprofile%
# C:\Program Files (x86)
%ProgramFiles(x86)%
# C:\Program Files (x86)\Common Files
%ProgramFiles(x86)%\Common Files
# C:\Program Files
%ProgramW6432%
# C:\Program Files\Common Files
%ProgramW6432%\Common Files
# C:\ProgramData
%ProgramData%
# C:\Users\test\AppData\Local\Temp
%temp%
```




### 2.  code

#### 2.1 if...else

- 等于 ==、equ
- 不等于 neq
- 大于 gtr
- 小于 lss
- 大于等于 geq
- 小于等于 leq

```bash
:: 将大于7的认定为失败
if %errorlevel% gtr 7 (
	echo error code: %errorlevel%
	exit -1
)
:: 将小于等于7认定为正常
if %errorlevel% leq 7 (
	echo error code: %errorlevel%
	exit 0
)
```
```bash
if ERRORLEVEL 1 (
	echo [ERROR] 请用管理员权限运行!!!
) else (
	goto install
)


# 忽略大小写比较字符串
if /i "%n%"=="1" goto dosome1
```

#### 2.2 for

> 在.bat文件中使用%%x，在cmd中使用%x

##### 2.2.1 遍历文件

```bash
for %%x in ("*.js") do (echo "%%x")
```

##### 2.2.2 遍历文件夹

```bash
for /d %%x in (*.*) do (echo "%%x")
```
##### 2.2.3 遍历数字
```bash
:: 创建c1到c10文件夹
for /l %%a in (1,1,10) do (mkdir c%%a)
```
##### 2.2.4 for循环中使用外部变量
```bash
setlocal enabledelayedexpansion
set v2=0
:: 要使用!v2!，不能用%v2%
FOR /l %%G in (1,1,5) Do (echo [!v2!] & set /a v2+=1)
```

#### 2.3 echo

```bash
# 输出内容
echo 123
# 输出空白行
echo.
# 命令执行成功或失败都不显示
task /f /im notepad* >NUL 2>NUL
# ^脱字符，用于输出&|等特殊符号
netstat -aon^|findstr 80 
```

#### 2.4 exist

> 支持文件或文件夹

```bash
# if exist
if exist c:\ (echo y) else (echo n)
# if not exist
if not exist c:\ (echo n) else (echo y)
```

#### 2.5 set

##### 2.5.1 set

```bash
:: 设置或修改变量
set v1=1
```

##### 2.5.2 set /a

```bash
:: 设置或修改变量，右侧为表达式
set /a v1=1*2*3
```

##### 2.5.3 set /p

```bash
:: 输入
set /p sel="select:"
```
##### 2.5.4 去除路径结尾斜杠

```bash
@echo off
set a1=c:\windows\system32\
::set a1=c:\windows\system32\test
set b1=%a1%
if "%a1:~-1%" equ "\" (set b1=%a1:~,-1%)
echo %a1%
echo %b1%
pause
```


#### 2.6 errorlevel

- 不区分大小写 errorlevel ERRORLEVEL
- 表示上一条命令的返回值

##### 2.6.1 单条语句 %errorlevel%

```bash
cmd1
echo %errorlevel%
```

```bash
if %ERRORLEVEL% neq 0 (echo "修改失败" && goto err)
```

##### 2.6.2 复合语句 !errorlevel!

- 复合语句时要开启变量延迟，否则errorlevel返回的值跟实际不一致(比如命令返回0,errorlevel值为2)
- 调用msbuild.exe时，最好也使用!errorlevel!

```bash
setlocal enabledelayedexpansion
for /f %%i in (文件列表.txt) do (
	%smart_ass% "%%i"
	echo !errorlevel!
)
setlocal disabledelayedexpansion
```

##### 2.6.3 比较

```bash
:: errorlevel 大于等于0 跳转sub1
if errorlevel 0 goto :sub1

:: errorlevel 小于0 跳转sub1
if not errorlevel 0 goto :sub1

:: errorlevel 不等于0 跳转sub1
:: EQU - 等于
:: NEQ - 不等于
:: LSS - 小于
:: LEQ - 小于或等于
:: GTR - 大于
:: GEQ - 大于或等于
if %errorlevel% neq 9 goto :sub1
```

#### 2.7 变量延迟

| 操作                             | 说明                                                         |
| -------------------------------- | ------------------------------------------------------------ |
| setlocal enabledelayedexpansion  | 开启变量延迟，复合语句一条一条的执行，!变量名!、%变量名%都可以，复合语句中必须使用!变量名!，使用%变量名%值会不变 |
| setlocal disabledelayedexpansion | 禁用变量延迟，复合语句按一条语句执行，只能%变量名%           |

```bash
@echo off
setlocal enabledelayedexpansion
set v1=6
set v2=0
echo %v1%
echo !v1!
echo 第一次for循环
FOR /l %%G in (1,1,5) Do (echo [!v2!] & set /a v2+=1)
echo 第二次for循环
FOR /l %%G in (1,1,5) Do (echo [%v2%] & set /a v2+=1)
pause
```

#### 2.8 注释

```bash
:: 注释1
rem 注释2
```

#### 2.9 退出

##### 2.9.1 exit

```bash
:: 退出cmd
exit
:: 退出当前脚本(比如1.bat调用2.bat，2.bat中执行exit /b)
exit /b
:: 退出当前脚本，并设置返回值为2
exit /b 2
```

##### 2.9.2 eof

> 退出当前脚本

```bash
:: 退出当前脚本
goto :eof
```

> 退出当前标，注意goto调用标签时，goto :eof会直接退出当前脚本

```bash
call :sub

:sub
echo aa
goto :eof
```

#### 2.10 call

##### 2.10.1 call 1.bat

```bash
:: 调用1.bat并等待1.bat执行完成
call 1.bat
```

##### 2.10.2 call 标签

```bash
@echo off
set _tst=0
rem 调用标签并传参
FOR /l %%G in (1,1,5) Do (call :sub %%G)
goto :eof

:sub
echo [%1] & set /a _tst+=1
goto :eof
```

#### 2.11 标签 goto

- call支持执行完返回
- goto不支持执行完返回

```bash
@echo off
rem 带参数调用sub2，执行完返回
call :sub2 666
rem 调用sub1，执行完返回
call :sub1
rem 调用sub1，执行完不返回
goto :sub1
rem 该语句不会执行
goto :sub3

:sub1
echo 标签1
goto :eof

:sub2
echo 标签2，参数：%1
goto :eof

:sub3
echo 标签3
goto :eof
```

#### 2.12 enabledelayedexpansion

```bash
# 延迟变量扩展
setlocal enabledelayedexpansion
setlocal disabledelayedexpansion

# 对于复合语句或者()中的语句，未开启时，会当作一条语句执行，先读取语句然后替换%变量%，再执行，会导致%errorlevel%始终为0的情况
# 解决方案一
setlocal enabledelayedexpansion
echo !errorlevel!

# 解决方案二
通过 if + goto 代替符合语句，例如：

# ---复合语句---
@echo off
set n=1
setlocal enabledelayedexpansion
if n neq 1 (
	testapp.exe 2
	echo !errorlevel!
)
setlocal disabledelayedexpansion
pause

# ---替换后的语句---
if /i "%n%"=="1" goto dosome1
:dosomeafter
pause
exit
:dosome1
testapp.exe 2
echo %errorlevel%
goto dosomeafter
```



### 3. 以管理员方式运行bat

#### 3.1 自动切换为管理员
方式一
```bash
@ECHO OFF & CD /D %~DP0 & TITLE 安装
>NUL 2>&1 REG.exe query "HKU\S-1-5-19" || (
    ECHO SET UAC = CreateObject^("Shell.Application"^) > "%TEMP%\Getadmin.vbs"
    ECHO UAC.ShellExecute "%~f0", "%1", "", "runas", 1 >> "%TEMP%\Getadmin.vbs"
    "%TEMP%\Getadmin.vbs"
    DEL /f /q "%TEMP%\Getadmin.vbs" 2>NUL
    Exit /b
)
```
方式二
```bash
@ECHO OFF&(PUSHD "%~DP0")&(REG QUERY "HKU\S-1-5-19">NUL 2>&1)||(
powershell -Command "Start-Process '%~sdpnx0' -Verb RunAs"&&EXIT)
```
#### 3.2 仅判断需要手动切换管理员
```bash
@echo off
whoami /groups | find "S-1-16-12288" >nul
if ERRORLEVEL 1 (echo [ERROR] 请用管理员权限运行!!!) else goto install
pause
exit

:install
pause
```

#### 3.3 runas

```bash
# 标准用户权限
runas /trustlevel:0x20000 "xxx.exe"
# 管理员权限
runas /trustlevel:0x40000  "xxx.exe"
```



### 4. 启动进程

#### 4.1 带参数
```bash
# 启动notepad，批处理退出
start "" "notepad.exe" c:\1.txt
```
#### 4.2 等待进程结束
```bash
# 启动notepad，等待notepad结束后，批处理再退出
start /wait /B "" "notepad.exe" c:\1.txt
```
#### 4.3 结束进程

```bash
taskkill /f /im TeamViewer* >NUL 2>NUL
```

#### 4.4 初始化环境变量，不关闭cmd

> 类似于VS开发者工具

- 无banner

```bash
set msbild_dir=C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\MSBuild\Current\Bin\
set path=%path%;"%msbild_dir%";
start "mytool" cmd.exe
# 此时可以使用msbuild命令
# 如果此时还是不能使用，可以把%path%放在行最后
```

- 有banner

```bash
set msbild_dir=C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\MSBuild\Current\Bin\
set path=%path%;"%msbild_dir%";
start "%~dp0logo.bat"
```

```bash
@echo off
title vs2019
echo banner
```

#### 4.5 管理员运行bat无法同时切换目录

```bash
@ECHO OFF & CD /D %~dp0
start cmd.exe
```

#### 4.6 explorer

```bash
# 打开文件夹
explorer C:\Program Files (x86)\Internet Explorer
# 选中文件
explorer /e,/select, C:\Program Files (x86)\Internet Explorer\iexplore.exe
# 选中文件夹
explorer /e,/select, C:\Program Files (x86)\Internet Explorer
```



### 5. 文件、文件夹

#### 5.1 删除文件
```bash
# 删除文件
del /q "TeamViewer*.txt" >NUL 2>NUL
```
#### 5.2 删除文件夹
```bash
# 删除文件夹
rd/s/q "%tmp%\TeamViewer"2>NUL
```
#### 5.3 删除文件夹（使用通配符）
```bash
# 删除pip-20.3.4.dist-info、pip-19.0.0.dist-info等文件夹
for /d %%x in ("pip-*dist-info") do rmdir /s /q "%%x"
```
#### 5.4 拷贝文件夹 xcopy
```bash
# 将文件夹LocalAppData\Icecream中的内容拷贝到%LocalAppData%\Icecream\文件夹中
# 第一个参数结尾不能有斜杠
# 第二个参数必须以斜杠结束
xcopy/e/y "LocalAppData\Icecream" "%LocalAppData%\Icecream\"
```

```bash
# 常见问题1：xcopy报错未知错误无法创建目录，原因：第二个参数结尾多了个斜杠
xcopy/e/y "D:\Release" "D:\Update\\"
# 改为
xcopy/e/y "D:\Release" "D:\Update\"
```

#### 5.5 拷贝文件夹 robocopy 小文件拷贝加速

> /e 复制子目录，包括空的子目录
>
> /s 复制子目录，但不复制空的子目录
>
> /copy:D 只拷贝文件的数据，不拷贝属性(比如是否只读)、所有者信息(文件权限)等，默认为copy:DAT
>
> /dcopy:D 只拷贝目录的数据，不拷贝属性(比如是否只读)、所有者信息(文件权限)等，默认为dcopy:DA
>
> /nodcopy:: 不复制任何目录信息
>
> *.js 只拷贝js文件，默认为\*.\*
>
> /mt:128 采用128线程进行拷贝，默认为8线程
>
> /r:0 失败后不重试，默认为重试100w次

> copyflag: D=数据，A=属性，T=时间戳，X=跳过替换数据流)。
>
> ​            (S=安全=NTFS ACL，O=所有者信息，U=审核信息

```bash
# 只复制js，文件和目录的数据部分、包含空目录
robocopy d\aaa C:\aaa *.js /e /copy:D /dcopy:D
# 复制所有文件，文件和目录的数据部分、包含空目录
robocopy d\aaa C:\aaa /e /copy:D /dcopy:D
# 复制所有文件，文件和目录的数据部分、不包含空目录、采用128线程、失败后不重试
robocopy d\aaa C:\aaa /s /dcopy:D /mt:128 /r:0
# 只拷贝目录结构
robocopy d\aaa C:\aaa /e /minage:19000101
```
- Robocopy 返回码 [https://qa.1r1g.com/sf/ask/2929130221/](https://qa.1r1g.com/sf/ask/2929130221/)
```bash
# 小于等于7代表成功，大于7代表失败
# 注意robocopy一般不会失败，别看错列
0没有文件被复制。没有遇到失败。没有文件不匹配。文件已存在于目标目录中；因此，跳过了复制操作。
1所有文件均已成功复制。
2目标目录中有一些源目录中不存在的附加文件。没有复制任何文件。
3复制了一些文件。存在附加文件。没有遇到失败。
5复制了一些文件。某些文件不匹配。没有遇到失败。
6存在其他文件和不匹配的文件。没有复制文件，也没有遇到任何故障。这意味着文件已经存在于目标目录中。
7文件已复制，存在文件不匹配，并且存在其他文件。
8几个文件没有复制。
```
```bash
# 常见错误
# 1.错误: 无效参数 #1:“D:\projects\Dll\"  D:\projects\bin\x86\Debug\Dll  *.* /E /Copy:D”
#   原因：源目录或目标目录结尾包含反斜杠
```



### 6. 注册表

- 项、值（值名称+值数据）
- ROOTKEY\SubKey
  - <u>HKCR</u>、<u>HKCU</u>、<u>HKLM</u>、HKU、HKCC
- /f 强制操作
- /v 值名称 /ve  默认值名称
- /t 类型
  - REG_SZ （不设置/t参数的默认类型）
  - REG_MULTI_SZ
  - REG_EXPAND_SZ
  - REG_DWORD "0x00000022"
  - REG_QWORD
  - REG_BINARY
  - REG_NONE
- /d 数据

#### 6.1 添加

```bash
# 添加
Reg Add HKCU\Software\TeamViewer /v UseNewUI /t REG_DWORD /d 1 /F >NUL 2>NUL
```

#### 6.2 删除

```bash
# 删除项
reg delete "HKCU\SOFTWARE\TeamViewer" /f >NUL 2>NUL
```



### 7. 服务

#### 7.1 停止服务

```bash
sc stop TeamViewer >NUL 2>NUL
sc delete TeamViewer >NUL 2>NUL
```



### 10. 创建桌面快捷方式

```bash
mshta VBScript:Execute("Set a=CreateObject(""WScript.Shell""):Set b=a.CreateShortcut(a.SpecialFolders(""Desktop"") & ""\TeamViewer.lnk""):b.TargetPath=""%~dp0TeamViewer.exe"":b.WorkingDirectory=""%~dp0"":b.Save:close")
```



### 11. 延时退出

```bash
# 延时2秒后退出
TIMEOUT /t 2 >NUL&exit
# 延时2秒后退出
ping 127.0.0.1 /n 2 >NUL&exit
```

### 12. 查看进程本地和远程端口 tcp udp port

```bash
# 忽略大小写搜索包含client的进城
tasklist|findstr /I "client"
# 查找进程使用的端口
netstat -ano|findstr /I "9836"
# 根据pid结束进城
taskkill /f /pid 9836
```

### 13. 查看开机时间

```bash
# 找到系统启动时间行
systeminfo
```

### 14. date

```bash
:: 20220729112417
set bat_date_time=%Date:~0,4%%Date:~5,2%%Date:~8,2%%Time:~0,2%%Time:~3,2%%Time:~6,2%
:: 20220729
set bat_date=%Date:~0,4%%Date:~5,2%%Date:~8,2%
```

### 15. editbin、dumpbin突破32位2g限制

> https://blog.csdn.net/u011430225/article/details/88950730
>
> 需要在vs命令行工具中打开

```bash
# 将32位程序xx.exe突破2g限制
editbin /largeaddressaware xx.exe
# 查看xx.exe是否突破了限制
# FILE HEADER VALUES中包含Application can handle large (>2GB) addresses，则表示已突破限制
dumpbin /headers xx.exe
```

### 16. dumpbin查看导出函数

```bash
# 查看dll文件
dumpbin /exports a.dll
# 查看lib文件
dumpbin /LINKERMEMBER aa.lib
```

### 17. 解除文件锁定

[Streams v1.6](https://docs.microsoft.com/zh-cn/sysinternals/downloads/streams)

> 问题：exe被锁定会导致程序无法启动；
>
> 原因：从网上下载的压缩包解压后的程序运行
>
> 现象1：dnspy调试时会显示报错信息：未能加载文件或程序集“xxx, Version=2.2.0.0, Culture=neutral, PublicKeyToken=null”或它的某一个依赖项。不支持操作。 (异常来自 HRESULT:0x80131515)
>
> 现象2：日志和事件查看器中一般不显示错误信息
> 
> 现象3：Fiddler启动报错：Failed to register Fiddler as the system proxy.

```bash
# 单个文件或批量解锁
streams.exe -nobanner -d 文件路径
streams.exe -nobanner -d -s 文件夹路径
```

### 18. 将zip文件隐藏到图片中

```bash
copy /b *.png+*.zip 3.png
```

### 19. 禁用或启用网卡

```bash
netsh interface set interface "WLAN" disabled
netsh interface set interface "WLAN" enable
```

### 20. bat中文乱码

```bash
文件编码改为ansi
```

### 21. vscode中终端中文乱码

```bash
# 将活动代码页改为utf-8
# chcp：change code page，用于查看或者修改当前活动代码页
# 936(中文)、65001(utf-8)
chcp 65001
```

### 22. 修改系统环境变量
```bash
SETX NODE_TLS_REJECT_UNAUTHORIZED 0 /M
```

### 23.  复制到剪切板

```bash
# 复制文字（第一个比第二个多个换行符）
echo 文本内容 | clip
echo|set /p="文本内容" | clip
# 复制文件内容
type 文件名.txt | clip
```

### 24. 删除字符串中的双引号

```bash
# 删除p1中的双引号，并赋值为p2
set "p2=%p1:"=%"
```

### 25. bat+vbs实现wps转pptx

```bash
@echo off
mshta VBScript:Execute("msgbox ""%~1"":msgbox ""%~2"":Set wpsApp = CreateObject(""KWPP.Application""):Set doc = wpsApp.Presentations.Open(""%~1""):savePath = ""%~2"":doc.SaveAs savePath, 24:doc.Close:wpsApp.Quit:Set doc = Nothing:Set wpsApp = Nothing:close")
```

```bash
# 说明
1. mshta.exe相当于一个ie内核的浏览器，可以打开网页或者执行js或者vbs
2. 打开网页
mshta.exe https://www.baidu.com
3. 执行js
mshta vbscript:window.execScript("alert('hello youniao');close()","javascript")
4. 执行vbs(注意：用冒号分割语句；用两个双引号代替一个双引号；用close关闭mshta窗口)
mshta VBScript:Execute("msgbox ""%~1"":close")
5. .bat/.vbs使用ansi编码，否则中文会乱码或者vbs提示错误未结束的错误字符串常量
```

常用com名称

| com名                  | com描述          |
| ---------------------- | ---------------- |
| KWPS.Aplication        | WPS文字          |
| KET.Application        | WPS的Excel       |
| KWPP.Application       | WPS的演示文档    |
| Word.Application       | Word             |
| Excel.Application      | Excel            |
| Powerpoint.Application | Powerpoint       |
| WScript.Shell          | 创建桌面快捷方式 |

### 26. 给文件增加只读属性

```bash
attrib +r "%appdata%\leidian9\cache"
```

### 27. 打开启动目录

```bash
shell:startup
```






### 88. 桌面右键-便捷工具

```reg
Windows Registry Editor Version 5.00

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT]
"Icon"="SHELL32.dll,-022"
"MUIVerb"="便捷工具(&A)"
"Position"="Bottom"
"SubCommands"=""

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell]

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\01kill explorer]
@="刷新Explorer"
"Icon"="%windir%\\System32\\SHELL32.dll,-16739"
"Position"="Top"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\01kill explorer\Command]
@="C:\\Windows\\restart.bat"


[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\02notepad]
"Icon"="notepad.exe"
"MUIVerb"="记事本"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\02notepad\Command]
@="notepad.exe"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\03cmd]
"Icon"="cmd.exe"
"MUIVerb"="命令提示符"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\03cmd\Command]
@="cmd.exe"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\04regedit]
"Icon"="regedit.exe"
"MUIVerb"="注册表编辑器"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\04regedit\Command]
@="regedit.exe"

;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\05calc]
;"Icon"="calc.exe"
;"MUIVerb"="计算工具"
;
;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\05calc\Command]
;@="calc.exe"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\07mspaint]
"Icon"="mspaint.exe"
"MUIVerb"="画图工具"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\07mspaint\Command]
@="mspaint.exe"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\08control]
"Icon"="control.exe"
"MUIVerb"="控制面板"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\08control\command]
@="control.exe"


[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\09网络连接]
@="网络连接"
"Icon"="C:\\Windows\\System32\\netshell.dll,0"
[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\09网络连接\Command]
@="explorer.exe shell:::{7007ACC7-3202-11D1-AAD2-00805FC1270E}"


[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\10Network]
"Icon"="SHELL32.dll,18"
"MUIVerb"="网络选择"
"Position"="Bottom"
"SubCommands"=""

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\10Network\Shell]

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\10Network\Shell\Wired_disabled]
@="禁用有线"
"Icon"="%windir%\\System32\\SHELL32.dll,131"
"Position"="Top"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\10Network\Shell\Wired_disabled\command]
@="netsh interface set interface 以太网 disabled"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\10Network\Shell\Wired_enabled]
@="启用有线"
"Icon"="%windir%\\System32\\SHELL32.dll,296"
"Position"="Top"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\10Network\Shell\Wired_enabled\command]
@="netsh interface set interface 以太网 enabled"


[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\10Network\Shell\Wireless_disabled]
@="禁用无线"
"Icon"="%windir%\\System32\\SHELL32.dll,131"
"Position"="Top"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\10Network\Shell\Wireless_disabled\command]
@="netsh interface set interface wlan disabled"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\10Network\Shell\Wireless_enabled]
@="启用无线"
"Icon"="%windir%\\System32\\SHELL32.dll,296"
"Position"="Top"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\10Network\Shell\Wireless_enabled\command]
@="netsh interface set interface wlan enabled"


;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\11Power]
;"Icon"="SHELL32.dll,-28"
;"MUIVerb"="电源"
;"Position"="Bottom"
;"SubCommands"=""
;
;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\11Power\Shell]
;
;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\11Power\Shell\关机]
;@="关机"
;"Icon"="C:\\Windows\\system32\\Shell32.dll,-28"
;"Position"="Top"
;
;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\11Power\Shell\关机\Command]
;@="C:\\Windows\\system32\\shutdown.exe -s -t 0"
;
;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\11Power\Shell\重新启动]
;@="重新启动"
;"Icon"="C:\\Windows\\system32\\Shell32.dll,-290"
;"Position"="Top"
;
;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\11Power\Shell\重新启动\Command]
;@="C:\\Windows\\system32\\Shutdown.exe -r -t 00"
;
;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\11Power\Shell\重启到高级选项]
;@="重启到高级选项"
;"Icon"="C:\\Windows\\system32\\Shell32.dll,-290"
;"Position"="Top"
;
;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\11Power\Shell\重启到高级选项\Command]
;@="C:\\Windows\\system32\\Shutdown.exe -r -o -f -t 00"


;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\12UAC设置]
;@="UAC设置"
;"Icon"="C:\\Windows\\system32\\UserAccountControlSettings.exe,0"
;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\12UAC设置\Command]
;@="UserAccountControlSettings.exe"


;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\13管理网络密码]
;@="管理网络密码"
;"Icon"="%SystemRoot%\\System32\\usercpl.dll,-1"
;[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\13管理网络密码\Command]
;@="explorer.exe shell:::{60632754-C523-4B62-B45C-4172DA012619}"


[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\11重启到高级选项]
@="重启到高级选项"
"Icon"="C:\\Windows\\system32\\Shell32.dll,-290"
"Position"="Top"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\11重启到高级选项\Command]
@="C:\\Windows\\system32\\Shutdown.exe -r -o -f -t 00"


[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\14远程桌面连接]
@="远程桌面连接"
"Icon"="%SystemRoot%\\system32\\mstsc.exe,0"
[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\14远程桌面连接\Command]
@="mstsc.exe"

[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\15设备和打印机]
@="设备和打印机"
"Icon"="%SystemRoot%\\system32\\devicecenter.dll,-1"
[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\15设备和打印机\Command]
@="explorer.exe shell:::{A8A91A66-3A7D-4424-8D24-04E180695C7A}"


[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\16管理工具]
@="管理工具"
"Icon"="%SystemRoot%\\System32\\imageres.dll,-114"
[HKEY_CLASSES_ROOT\DesktopBackground\Shell\Appearance_WAT\Shell\16管理工具\Command]
@="explorer.exe shell:::{D20EA4E1-3957-11D2-A40B-0C5020524153}"
```



### 99. 样例文件

```bash
:: 管理员身份运行
@ECHO OFF&(PUSHD "%~DP0")&(REG QUERY "HKU\S-1-5-19">NUL 2>&1)||(
powershell -Command "Start-Process '%~sdpnx0' -Verb RunAs"&&EXIT)

:: xp检测
ver|findstr "5\.[0-9]\.[0-9][0-9]*" > NUL && (
ECHO.&ECHO 当前版本不支持WinXP &PAUSE>NUL&EXIT)

:: 关闭进程
taskkill /f /im XMP.exe >NUL 2>NUL
taskkill /f /im XLLiveUD* >NUL 2>NUL

:: 删除文件和文件夹
:: %temp%
:: %UserProfile%\AppData
:: %AppData%
:: %userprofile%\Documents
:: %PUBLIC%\Documents
:: %ProgramData%
:: %ProgramW6432%\Common Files
:: %ProgramFiles(x86)%\Common Files
rd/s/q "%TEMP%\Xmp"2>NUL
del/q "%ProgramData%\APlayerCodecs3.exe" >NUL 2>NUL

:: 创建文件夹
md "%PUBLIC%\Thunder Network\cid_store.dat" 2>NUL

:: 添加注册表
IF NOT EXIST "%ProgramW6432%" (
reg add "HKLM\SOFTWARE\Thunder Network\ThunderOem\thunder_backwnd" /f /v "dir" /d "%~dp0\" >NUL 2>NUL
) ELSE (
reg add "HKLM\SOFTWARE\Wow6432Node\Thunder Network\ThunderOem\thunder_backwnd" /f /v "Path" /d "%~dp0Program\Thunder.exe" >NUL 2>NUL
)

:: 注册dll
regsvr32 /s BHO\ThunderAgent.dll

::创建桌面快捷方式
mshta VBScript:Execute("Set a=CreateObject(""WScript.Shell""):Set b=a.CreateShortcut(a.SpecialFolders(""Desktop"") & ""\迅雷.lnk""):b.TargetPath=""%~sdp0Program\Thunder.exe"":b.WorkingDirectory=""%~sdp0Program"":b.Save:close")

ECHO.&ECHO 完成 &TIMEOUT /t 2 >NUL&EXIT
```

