
# Electron

## 1. Node

### 1.1 不使用-g安装如果调用

- 方式一：

```bash
node_modules\.bin\electron -v
node_modules\.bin\electron-packager .
node_modules\.bin\electron-builder
```

- 方式二：

```bash
# package.json
{
	"scripts":{
		"start": "electron ."
	}
}
```



### 1.2 运行.js文件 

```bash
node index.js
```

### 1.3 uuid包报错

> Package subpath './v4' is not defined by "exports" in node_modules\uuid\package.json

- 错误用法

```bash
const uuidv4 = require('uuid/v4');
uuidv4();
```

- 正确用法

```bash
const uuid = require('uuid');
uuid.v4();
```

### 1.4 使用本地依赖包

```json
{
  "dependencies": {
    "@baidu/vuex-electron": "file:./node_modules_local/@baidu/vuex-electron"
  }
}
```



## 2. 包管理工具

### 2.1 yarn

```bash
# 安装yarn
npm install -g yarn
# 设置淘宝镜像
yarn config set ELECTRON_MIRROR https://npm.taobao.org/mirrors/electron/
```

```bash
# 初始化项目
yarn init
# 安装项目依赖
yarn install
# 启动项目
yarn start
# 安装单个依赖
yarn add electron-packager
```

#### 2.1.1 安装依赖到devDependencies

```bash
yarn add electron -dev
# 或者
yarn add electron -D
```

####  2.1.2 安装指定版本的包

````bash
yarn add electron@13.6.9
````

#### 2.1.3 更新为指定版本的包

```bash
yarn upgrade electron@20.0.3
```

或

```bash
# 修改package.json中的版本号
yarn upgrade electron
```

#### 2.1.4 版本号范围

```bash
# 版本号前两位不变
~2.0.0
# 版本号第一位不拜年
^2.0.0
# x前面的版本号不变
1.x
# 任意版本
*
# 两个版本号之间
1.0.1-1.5.9
```

#### 2.1.5 yarn在powershell中无法运行问题

```bash
# yarn : 无法加载文件 yarn.ps1，因为在此系统上禁止运行脚本。
# 解决：用管理员方式打开powershell，执行以下语句
set-executionpolicy remotesigned
```



### 2.2 npm

```bash
类似
```



## 3. electron

### 3.1 打包

#### 3.1.1 打绿色包

```bash
# 安装electron-packager
yarn add electron-packager
set ELECTRON_MIRROR=http://npm.taobao.org/mirrors/electron/
# 打包
# app1为.exe名称；../installer为输出目录
node_modules\.bin\electron-packager . app1 --win --out ../installer --arch=x64 
```

#### 3.1.2 nsis安装包

##### 3.1.2.1 打包

- 安装

```bash
yarn add electron-builder --dev
```

- 配置package.json中的scripts节点，用于执行打包命令

```js
"scripts": {
    "dist":"electron-builder"
}
```

- 配置package.json中的build节点

```js
"build": {
    "productName":"xxxx",		//项目名 这也是生成的exe文件的前缀名
    "appId": "com.example.yourapp",	//包名  
    "copyright":"xxxx",			//版权信息
    "directories": { 			//输出文件夹
      "output": "build"
    }, 
    "nsis": {	//nsis相关配置，打包方式为nsis时生效
      "guid": "",										// 固定注册表安装包，一般不设置
      "oneClick": false, 								// 是否一键安装
      "allowElevation": true, 							// 允许请求提升，如果为false，则用户必须使用提升的权限重新启动安装程序。
      "allowToChangeInstallationDirectory": true, 		// 允许修改安装目录
      "installerIcon": "./build/icons/aaa.ico",			// 安装图标
      "uninstallerIcon": "./build/icons/bbb.ico",		//卸载图标
      "installerHeaderIcon": "./build/icons/aaa.ico", 	// 安装时头部图标
      "createDesktopShortcut": true, 					// 创建桌面图标
      "createStartMenuShortcut": true,					// 创建开始菜单图标
      "shortcutName": "xxxx", 							// 图标名称
      "include": "build/script/installer.nsh", 			// 包含的自定义nsis脚本
    },
    "publish": [
      {
        "provider": "generic", 		// 服务器提供商，也可以是GitHub等等
        "url": "http://xxxxx/" 		// 服务器地址
      }
    ],
    "win": {
      "icon": "build/icons/aims.ico",
      "target": [
        {
          "target": "nsis",			//使用nsis打成安装包，"portable"打包成免安装版
          "arch": [
            "ia32",				//32位
            "x64" 				//64位
          ]
        }
      ]
    },
    "mac": {
      "icon": "build/icons/icon.icns"
    },
    "linux": {
      "icon": "build/icons"
    }
  }
```

- 打包

```bash
yarn dist
```



##### 3.1.2.2 注册表位置变化问题

> 优先级：build.nsis.guid、build.appId、name

> com.example.yourapp 对应的 guid 1fb8a7aa-e784-579d-a981-2142d5507b97

- electron-builder源码分析：

```bash
# .\node_modules\app-builder-lib\templates\nsis\multiUser.nsh
!define /ifndef UNINSTALL_REGISTRY_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${UNINSTALL_APP_KEY}"

# .\node_modules\app-builder-lib\out\targets\nsis\NsisTarget.js
# appInfo.id格式：com.electron.${name} 或 ${build.appId}
const guid = options.guid || UUID.v5(appInfo.id, ELECTRON_BUILDER_NS_UUID)
    const uninstallAppKey = guid.replace(/\\/g, " - ")
    const defines: Defines = {
      UNINSTALL_APP_KEY: uninstallAppKey,
    }
```

- 解决方案

  - 方案一：

  ```bash
  # package.json中的build.appId或name属性保持不变
  ```

  - 方案二：

  ```bash
  # package.json中自定义build.nsis.guid值
  ```

##### 3.1.2.3 打包时下载资源慢

```bash
# 浏览器引擎驱动镜像
yarn config set chromedriver_cdnurl https://mirrors.huaweicloud.com/chromedriver
# electron镜像
yarn config set electron_mirror https://mirrors.huaweicloud.com/electron/
# electron_builder_binaries镜像
yarn config set electron_builder_binaries_mirror https://mirrors.huaweicloud.com/electron-builder-binaries/
# yarn镜像
yarn config set registry https://mirrors.huaweicloud.com/repository/npm/
```

##### 3.1.2.4 将文件夹打包到安装包，并输出到resources目录中

```json
{
  "build": {
    "extraFiles": [
      "./resources/DLWrapper",
      "./resources/vbs"
    ]
  }
}
```



###　3.2 反编译

#### 3.2.1 js格式化

> 单个文件

```bash
# 安装uglify-js
npm install -g uglify-js 
# 格式化main.js并输出_main.js，可以直接替换原文件使用
uglifyjs main.js -b -o _main.js
```

> 批量

```bash
for %x in ("*.js") do (uglifyjs "%x" -b -o "%x")
```

#### 3.2.2 打开开发者工具

```js
u = new y.BrowserWindow(s),
u.loadURL(l(n)), u.webContents.on("did-finish-load", function() {});
```

改为

```js
// mode值：right、left、bottom、detach、undocked
u = new y.BrowserWindow(s),
u.loadURL(l(n)),u.webContents.openDevTools({mode:'detach'}), u.webContents.on("did-finish-load", function() {});
```

说明：

```bash
开发 dev 状态的话，Electron 的开发者工具快捷键：ctrl+shift+i。
```



#### 3.2.3 查看electron版本号

```bash
# 开发者工具，控制台
# 可以查看chrome、electron、node等版本号
process.versions
```



